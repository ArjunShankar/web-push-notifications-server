const MsgModel = require('../models/message.model');
const SubscriberModel = require('../models/subscriber.model');
const CONFIG = require('../config/config');
const HTTP_CONSTANTS = require('../config/httpStatusConstants');
const webpush = require('web-push');

webpush.setVapidDetails(
    'mailto:xyz@abc.com',
    CONFIG.vapidPublicKey,
    CONFIG.vapidPrivateKey
);

//TODO save the messages sent ... in the msg model

module.exports.sendMessage = (request,response) => {
    var payload = request.body;
    const notificationPayload = {
        "notification": {
            "title": payload.message,
            "body": "But the network was unreachable !!! ",
            "data": {
                "dateOfArrival": Date.now(),
                "primaryKey": 1
            }
        }
    };

    SubscriberModel.find().lean().exec(function(err,res){
        if(err){
            response.sendStatus(500);
        }
        else if(res){
            Promise.all(res.map(row => webpush.sendNotification(
                row.subscription, JSON.stringify(notificationPayload) )))
                .then(() => response.status(200).json({message: 'Notification sent successfully.'}))
                .catch(err => {
                    response.status(500).json({error:err});
                });
        }
    });
}

sendOutNotifications = (listOfSubscribers,msg) => {
    

    

}

