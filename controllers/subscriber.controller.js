let SubscriberModel = require('../models/subscriber.model');
let HTTP_CONSTANTS = require('../config/httpStatusConstants');

module.exports.addSubscriber = (request,response) => {
    var subscriber = new SubscriberModel(request.body); //TODO deally we should validate the input before passing it on to mongo. Prevent bad data and injection attacks.
    subscriber.save(function(err,res){
        if(err){
            return response.status(HTTP_CONSTANTS.BAD_REQUEST).send({
                message:err
            });
        }
        else if(res){
            return response.status(HTTP_CONSTANTS.CREATED).send({
                message:'Row created.'
            })
        }

    })
}


