const mongoose = require('mongoose');

let msgSchema = new mongoose.Schema({
        message:{
            type:String,
            required:true,
        }
    });

let Message = mongoose.model('message',msgSchema);
module.exports = Message;    