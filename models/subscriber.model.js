const mongoose = require('mongoose');
let subscriberSchema = new mongoose.Schema({
        action:String,
        subscription:{}
    });

subscriberSchema.set('toJSON', {
        virtuals: true,
        versionKey:false,
        transform: function (doc, ret) {   
            delete ret._id;
            delete ret.__v;  }
      });

let Subscriber = mongoose.model('subscriber',subscriberSchema);
module.exports = Subscriber;