var express = require('express');
var router = express.Router();

var subscriberController = require('../controllers/subscriber.controller');
var msgController = require('../controllers/message.controller');


router.use(function timeLog (req, res, next) {
  console.log('URL Called: %s | time: ',req.originalUrl, Date.now());
  var headerStr = "Content-Type,";
        for (var headerKey in req.headers) {
            if (headerKey == 'access-control-request-headers') {
                headerStr = headerStr + req.header(headerKey);
            } else {
                headerStr = headerStr + headerKey;
            }
            headerStr = headerStr + ",";
        }
  res.header('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
  res.header("Access-Control-Allow-Headers", headerStr);
  next();
})

router.post('/api/subscriber', subscriberController.addSubscriber);
router.post('/api/message', msgController.sendMessage);


router.get('/test', function(err,res){
    res.status(200).send({msg:'Express app is up.This is an unsecured test api.'});
});

module.exports = router;
