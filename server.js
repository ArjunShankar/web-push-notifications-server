const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const routes = require('./routes');
const CONFIG = require('./config/config');

let mongoDBUrl = 'mongodb://' + CONFIG.mongoHost+':'+CONFIG.mongoPort + '/' + CONFIG.mongoSchema;
mongoose.Promise = global.Promise;

let connectWithRetry = () =>{
    mongoose.connect(mongoDBUrl).then(
        () => {
            console.log("MongoDB connection established");
            setupServer();
        },
        err => {
            console.log(" MongoDB connection error. Please make sure MongoDB is running. ");
            setTimeout(connectWithRetry,2000);
        }
    );
}

let setupServer = () => {
        var app = express();
        app.set('port', CONFIG.port || 3000);
        app.use(cors());
        app.use(bodyParser.urlencoded({extended:true}));
        app.use(bodyParser.json());
        app.listen(app.get('port'), () => {
            console.log("App is running at http://localhost: %d in %s mode", app.get('port'), app.get('env'));
            console.log('Press CTRL-C to stop\n');
        });
        
        app.use(routes);
        module.exports = app; 
}

connectWithRetry();     // this will keep on trying to get the mongo connection until success.

